Scrivere un programma che attiva una serie di thread per effettuare il calcolo approssimato di π. 

Il programma principale prende come input da riga di comando un parametro che indica il grado di accuratezza (accuracy) per il calcolo di π e il tempo massimo di attesa dopo cui il main interrompe tutti i thread. 

I thread vengono utilizzati (cercando di massimizzare il numero di operazioni effettuate nel lasso di tempo a disposizione) per effettuare le seguenti operazioni per il calcolo di π tramite la serie di Gregory-Leibniz: π = 4/1 − 4/3 + 4/5 − 4/7 + 4/9 − 4/11 ...
     
I thread vengono tutti interrotti quando una delle due condizioni seguenti risulta verificata: 	1) il tempo massimo di attesa è stato superato; 	2) la differenza tra il valore stimato di π e il valore Math.PI è minore di accuracy.