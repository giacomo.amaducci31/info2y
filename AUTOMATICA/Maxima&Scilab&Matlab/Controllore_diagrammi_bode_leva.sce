//CONTROLLORE DIAGRAMMI DI BODE //

function VerifySynthesisBodeCT(Grat,Gdel,R,wmin,ndec,wcmin,wcmax,pmmin,wa1,wa2,AadB,wr1,wr2,Ardb,nfig)
	dmin	= floor(log10(wmin));
	w	= logspace(dmin,dmin+ndec,500);
	f	= w/2/%pi;
	hGrat	= repfreq(Grat,f);
	hR	= repfreq(R,f);
	[mLdB,pLdeg] = dbphi(hR.*hGrat);
	pLdeg = pLdeg-w*Gdel*180/%pi;
	[pm,fc] = p_margin(R*Grat);
	wc = 2*%pi*fc;
	pm = pm-wc*Gdel*180/%pi;
	scf(nfig); clf;
	drawlater();
	subplot(211);
		plot(w, mLdB, 'b');
		plot(w, zeros(w), 'k:');
		plot([wcmin,wcmin],[-5,+5], 'r');
		plot([wcmax,wcmax],[-5,+5], 'g');
		if wa2-max(wa1,wmin)>0 & AadB > 0
			xrect([max(wa1,wmin), AadB, wa2-max(wa1,wmin), AadB]);
		end;
		if min(wr2, max(w))-wr1>0 & -ArdB>0
			xrect([wr1,0,min(wr2,max(w))-wr1, -ArdB]);
		end;
		ax = get ("current_axes");
		ax.log_flags = "ln";
		ylabel("|L(jw)|, dB");
	subplot(212);
		plot(w, pLdeg, 'b');
		plot(w, -180*ones(w), 'r');
		plot(w, (pmmin-180)*ones(w),'c');
		plot([wc,wc],[-180,pm-180],'g');
		ax = get ("current_axes");
		ax.log_flags = "ln";
		xlabel("w (r/s)");
		ylabel("arg(L(jw)), deg");
		title(sprintf("wc = %f r/s, pm = %f deg", wc, pm));
	drawnow();
endfunction

s=%s;
//Funzione di trasferimento del processo
Grat = syslin('c',20/(1+s)/(1+0.2*s));
//Ritardo (è il denominare di e^-...
Gdel = 0;
//Funzione di trasferimento del regolatore da noi calcolato
R=syslin('c', 0.03/s);
//intervallo in cui sta la pulsazione critica
wcmin = 0.4;
wcmax = 2;
//margine di fase richiesto
pmmin = 45;
//fra cosa e cosa deve stare wa
wa1=-%inf;
wa2=%inf;
//valore minimo ammesso per |L| in [wa1, wa2] - usare un numero o %nan se non disponibile
AadB = %nan;
//fra cosa e cosa deve stare wr
wr1 = 20;
wr2 = 50;
//valore minimo ammesso per |L| in [wr1, wr2] - usare un numero o %nan se non disponibile
ArdB = -30;
//default per disegnare grafico
wmin = 0.01;
ndec = 4;
nfig = 0;

VerifySynthesisBodeCT(Grat,Gdel,R,wmin,ndec,wcmin,wcmax,pmmin,wa1,wa2,AadB,wr1,wr2,ArdB,nfig);