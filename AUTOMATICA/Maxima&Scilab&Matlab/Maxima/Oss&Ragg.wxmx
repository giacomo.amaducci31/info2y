PK     `�N�B�H         mimetypetext/x-wxmathmlPK     `�N�f1S  S  
   format.txt

This file contains a wxMaxima session in the .wxmx format.
.wxmx files are .xml-based files contained in a .zip container like .odt
or .docx files. After changing their name to end in .zip the .xml and
eventual bitmap files inside them can be extracted using any .zip file
viewer.
The reason why part of a .wxmx file still might still seem to make sense in a
ordinary text viewer is that the text portion of .wxmx by default
isn't compressed: The text is typically small and compressing it would
mean that changing a single character would (with a high probability) change
big parts of the  whole contents of the compressed .zip archive.
Even if version control tools like git and svn that remember all changes
that were ever made to a file can handle binary files compression would
make the changed part of the file bigger and therefore seriously reduce
the efficiency of version control

wxMaxima can be downloaded from https://github.com/wxMaxima-developers/wxmaxima.
It also is part of the windows installer for maxima
(https://wxmaxima-developers.github.io/wxmaxima/).

If a .wxmx file is broken but the content.xml portion of the file can still be
viewed using an text editor just save the xml's text as "content.xml"
and try to open it using a recent version of wxMaxima.
If it is valid XML (the XML header is intact, all opened tags are closed again,
the text is saved with the text encoding "UTF8 without BOM" and the few
special characters XML requires this for are properly escaped)
chances are high that wxMaxima will be able to recover all code and text
from the XML file.

PK     `�N���v�  �     content.xml<?xml version="1.0" encoding="UTF-8"?>

<!--   Created using wxMaxima 19.01.2x   -->
<!--https://wxMaxima-developers.github.io/wxmaxima/-->

<wxMaximaDocument version="1.5" zoom="100" activecell="11">

<cell type="code">
<input>
<editor type="input">
<line>A:matrix([-1, 0], [0, -1]);</line>
</editor>
</input>
<output>
<mth><lbl userdefined="yes" userdefinedlabel="A">(%o1) </lbl><tb roundedParens="true"><mtr><mtd><v>−</v><n>1</n></mtd><mtd><n>0</n></mtd></mtr><mtr><mtd><n>0</n></mtd><mtd><v>−</v><n>1</n></mtd></mtr></tb>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>b:matrix([1,1]);</line>
</editor>
</input>
<output>
<mth><lbl userdefined="yes" userdefinedlabel="b">(%o2) </lbl><tb roundedParens="true"><mtr><mtd><n>1</n></mtd><mtd><n>1</n></mtd></mtr></tb>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>bt:transpose(b);</line>
</editor>
</input>
<output>
<mth><lbl userdefined="yes" userdefinedlabel="bt">(%o3) </lbl><tb roundedParens="true"><mtr><mtd><n>1</n></mtd></mtr><mtr><mtd><n>1</n></mtd></mtr></tb>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>MR:addcol(bt, A.bt);</line>
</editor>
</input>
<output>
<mth><lbl userdefined="yes" userdefinedlabel="MR">(%o4) </lbl><tb roundedParens="true"><mtr><mtd><n>1</n></mtd><mtd><v>−</v><n>1</n></mtd></mtr><mtr><mtd><n>1</n></mtd><mtd><v>−</v><n>1</n></mtd></mtr></tb>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>determinant(MR);</line>
</editor>
</input>
<output>
<mth><lbl>(%o5) </lbl><n>0</n>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>A2:matrix([-1, 0], [0, 4]);</line>
</editor>
</input>
<output>
<mth><lbl userdefined="yes" userdefinedlabel="A2">(%o6) </lbl><tb roundedParens="true"><mtr><mtd><v>−</v><n>1</n></mtd><mtd><n>0</n></mtd></mtr><mtr><mtd><n>0</n></mtd><mtd><n>4</n></mtd></mtr></tb>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>A2t:transpose(A2);</line>
</editor>
</input>
<output>
<mth><lbl userdefined="yes" userdefinedlabel="A2t">(%o7) </lbl><tb roundedParens="true"><mtr><mtd><v>−</v><n>1</n></mtd><mtd><n>0</n></mtd></mtr><mtr><mtd><n>0</n></mtd><mtd><n>4</n></mtd></mtr></tb>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>c:matrix([0,1]);</line>
</editor>
</input>
<output>
<mth><lbl userdefined="yes" userdefinedlabel="c">(%o8) </lbl><tb roundedParens="true"><mtr><mtd><n>0</n></mtd><mtd><n>1</n></mtd></mtr></tb>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>ct:transpose(c);</line>
</editor>
</input>
<output>
<mth><lbl userdefined="yes" userdefinedlabel="ct">(%o9) </lbl><tb roundedParens="true"><mtr><mtd><n>0</n></mtd></mtr><mtr><mtd><n>1</n></mtd></mtr></tb>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>MO:addcol(ct, A2t.ct);</line>
</editor>
</input>
<output>
<mth><lbl userdefined="yes" userdefinedlabel="MO">(%o10) </lbl><tb roundedParens="true"><mtr><mtd><n>0</n></mtd><mtd><n>0</n></mtd></mtr><mtr><mtd><n>1</n></mtd><mtd><n>4</n></mtd></mtr></tb>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>determinant(MO);</line>
</editor>
</input>
<output>
<mth><lbl>(%o11) </lbl><n>0</n>
</mth></output>
</cell>

</wxMaximaDocument>PK      `�N�B�H                       mimetypePK      `�N�f1S  S  
             5   format.txtPK      `�N���v�  �               �  content.xmlPK      �   �    